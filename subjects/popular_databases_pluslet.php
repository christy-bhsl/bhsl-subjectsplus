<!-- start pluslet -->
<!-- Updated 10-19-2015 -->
<div class="pluslet">
  <div class="titlebar">
    <div class="titlebar_text">Popular Databases</div>
  </div>
  <div class="pluslet_body">
   <h3 class="listTitle">Dentistry</h3>
  <ul>
      <li><a href="//go.openathens.net/redirector/bswhealth.com?url=https%3A%2F%2Fonline.statref.com%2FEntry.aspx%3Flt%3DAnatomy%26grpalias%3DBHSL" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'AnatomyTVDent');"target="_blank"
title="opens new window">Anatomy.TV - Dentistry</a></li>
      <li><a href="//www.boardvitals.com/users/sign_in" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'BoardVitals');"target="_blank" title="opens new window">BoardVitals</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=ehost&defaultdb=ddh&groupid=main" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'DentistryOralSci');"target="_blank" title="opens new window">Dentistry & Oral Sciences Source</a></li>
  <li><a href="go.openathens.net/redirector/bswhealth.com?url=https%3A%2F%2Fwidgets.ebscohost.com%2Fprod%2Fcustomlink%2Foasession%2Fgo.html%3Fhttp%3A%2F%2Fonline.lexi.com%2Flco%2Foa%2Fauth.jsp%23" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'LexiCompDentistry');"target="_blank" title="opens new window">Lexicomp Online for Dentistry</a></li>
  <li>(College of Dentistry Only)</li>

  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Fwww.visualdx.com%2Fvisualdx" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'VisualDx');"target="_blank" title="opens new window">VisualDx</a></li>
  </ul>
   <h3 class="listTitle">Medicine</h3>
	<ul>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=https://www.clinicalkey.com" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'ClinicalKey');"target="_blank" title="opens new window">ClinicalKey</a></li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Fonline.epocrates.com" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'Epocrates');"target="_blank" title="opens new window">Epocrates Online Premium</a></li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=https%3A%2F%2Fonline.lexi.com%2Flco%2Faction%2Fhome%3Fsiteid%3D1" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'Lexicomp');"target="_blank" title="opens new window">Lexicomp</a></li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=https%3A%2F%2Fnaturalmedicines.therapeuticresearch.com" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'NatStandard');"target="_blank" title="opens new window">Natural Medicines</a></li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Fovidsp.ovid.com%2Fovidweb.cgi%3FT%3DJS%26NEWS%3Dn%26CSC%3DY%26PAGE%3Dmain%26D%3Dmezz" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'OvidMedline');"target="_blank" title="opens new window">Ovid Medline</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=ehost&defaultdb=psyh&groupid=main" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'PsycINFO');"target="_blank" title="opens new window">PsycINFO</a></li>
  <li><a href="//www.ncbi.nlm.nih.gov/entrez/query.fcgi?otool=txbcdlib" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'PubMed');"target="_blank" title="opens new window">PubMed</a></li>
  <li><a href="//www.uptodate.com/online" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'UpToDate');"target="_blank" title="opens new window">UpToDate Anywhere</a> </li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Fwww.visualdx.com%2Fvisualdx" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'VisualDx');"target="_blank" title="opens new window">VisualDx</a></li>
  </ul>
  <h3 class="listTitle">Nursing &amp; Allied Health</h3>
  <ul>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Ffod.infobase.com%2FPortalPlaylists.aspx%3FwID%3D240929%26sid%3D1587%26level%3DSubject" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'FilmsOnDemand');"target="_blank" title="opens new window">Films on Demand – Nursing</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=cinahladv&defaultdb=ccm&groupid=main" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'CINAHL');"target="_blank" title="opens new window">CINAHL Complete</a></li>
  <li><a href="//go.openathens.net/redirector/bswhealth.com?url=http%3A%2F%2Fovidsp.ovid.com%2Fovidweb.cgi%3FT%3DJS%26NEWS%3Dn%26CSC%3DY%26PAGE%3Dmain%26D%3Djbi" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'JoannaBriggs');"target="_blank" title="opens new window">Joanna Briggs Inst. EBP</a></li>
  <li><a href="//login.mns.elsevierperformancemanager.com/Login.aspx?VirtualName=bhcs-txdallas">Mosby's Nursing Skills</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=nup&groupid=main">Nursing Reference Center Plus</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=rrc&groupid=main" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'RehabRef');"target="_blank" title="opens new window">Rehabilitation Reference Center</a></li>
  <li><a href="//search.ebscohost.com/login.aspx?authtype=shib&custid=s5616422&profile=swrc&groupid=main" onClick="ga('send', 'event', 'OutboundLink', 'ClickLink', 'SocialWorkRef');"target="_blank" title="opens new window">Social Work Reference Center</a></li>
  </ul>
  </div>
</div>
<!-- end pluslet -->
