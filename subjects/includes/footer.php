</div>


<div class="row footer">

    <div class="span3 footerblocks"> <a href="https://dentistry.tamhsc.edu/"><img src="//bhslibrary.tamhsc.edu/wp-content/themes/bcdlibrary/img/bcd.png" alt="Texas A&amp;M College of Dentistry Logo" /> </a></div>
    <div class="span3 center footerblocks">
      <p>Follow Us <a href="https://twitter.com/BHS_LIB"><img src="//bhslibrary.tamhsc.edu/wp-content/themes/bcdlibrary/img/twitter.png" alt="Twitter Logo" /></a><a href="https://www.facebook.com/BHSLIB"><img src="//bhslibrary.tamhsc.edu/wp-content/themes/bcdlibrary/img/facebook.png" alt="Facebook Logo" /></a></p><br />
      <p>Dental: 214-828-8151  |   Medical: 214-820-2377</p>
      <p><span class="copyright">Copyright Baylor Health Sciences Library. All rights reserved.</span></p>
    </div>
    <div class="span3 footerblocks" style="padding-top:2%"> <a href="http://www.baylorhealth.com/Pages/Default.aspx"><img src="//bhslibrary.tamhsc.edu/wp-content/themes/bcdlibrary/img/baylorswwhite.png" alt="Baylor Scott and White Health Care System Logo" /> </a></div>
  </div>
</div>





</body>
<script src="//bhslibrary.tamhsc.edu/sp/assets/jquery/bootstrap.js"></script>
</html>
