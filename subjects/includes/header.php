<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php print $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="apple-touch-icon" sizes="76x76" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/76x76ipad2andmini.png" >
<link rel="apple-touch-icon" sizes="120x120" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/120x120iphone.png" >
<link rel="apple-touch-icon" sizes="152x152" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/152x152ipadandmini.png" >
<link rel="apple-touch-icon" sizes="167x167" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/167x167ipadpro.png" >
<link rel="apple-touch-icon" sizes="180x180" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/180x180iphoneplus.png" >
<link rel="icon" sizes="192x192" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/192x192androidlg.png" >
<link rel="icon" sizes="128x128" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/128x128androidsm.png" >
<link rel="apple-touch-icon" sizes="128x128" href="https://bhslibrary.tamhsc.edu/wp-content/uploads/2015/10/128x128androidsm.png" >
<meta name="Description" content="<?php if (isset($description)) {print $description;} ?>" />
<meta name="Keywords" content="<?php if (isset($keywords)) {print $keywords;} ?>" />
<meta name="Author" content="" />

<!--<link type="text/css" media="all" rel="stylesheet" href="https://bhslibrary.tamhsc.edu.library.baylorhealth.edu/sp/assets/css/bootstrap.css"/>-->
<link type="text/css" media="all" rel="stylesheet" href="https://bhslibrary.tamhsc.edu/sp/assets/css/bootstrap.css"/>
<link type="text/css" media="screen" rel="stylesheet" href="https://bhslibrary.tamhsc.edu/sp/assets/css/bcdlcustom.css">


<!-- <link type="text/css" media="print" rel="stylesheet" href="<?php print $AssetPath; ?>css/print.css"> -->

<?php // Load our jQuery libraries + some css
if (isset($use_jquery)) { print generatejQuery($use_jquery);
?>
    <script type="text/javascript" language="javascript">
    // Used for autocomplete

        $.fn.defaultText = function(value){

            var element = this.eq(0);
            element.data('defaultText',value);

            element.focus(function(){
                if(element.val() == value){
                    element.val('').removeClass('defaultText');
                }
            }).blur(function(){
                if(element.val() == '' || element.val() == value){
                    element.addClass('defaultText').val(value);

                }
            });

            return element.blur();
        }
    </script>
<?php
}
?>
</head>

<body>

<div class="container">
      <div class="row header"><a href="https://bhslibrary.tamhsc.edu/" id="logo">
        <span class="span3 logo"><img src="https://bhslibrary.tamhsc.edu/wp-content/themes/bcdlibrary/img/bcdllogo.png" alt="BHS Library Logo" /></span>
        <span class="span9 tagline">Search. Discover. Share.</span></a>
      </div>
      <div class="row main">
	<div class="span12">
  	<div><h1><?php print $page_title; ?></h1></div>

</div>
